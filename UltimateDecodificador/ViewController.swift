//
//  ViewController.swift
//  UltimateDecodificador
//
//  Created by Rafael Gonzalez on 24/9/17.
//  Copyright © 2017 Rafael Gonzalez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    @IBOutlet var pickerViewOutlet: UIPickerView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var lockButton: UIButton!
    @IBOutlet var unlockButton: UIButton!

    
    let array = [["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                 ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                 ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]]

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func infoButton(_ sender: UIButton)
    {
        
    }
    
    @IBAction func lockButtonAction(_ sender: UIButton)
    {
        
    }
    
    @IBAction func unlockButtonAction(_ sender: UIButton)
    {
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return array.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return array[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return array[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0))
        let label = UILabel(frame: CGRect(x: 5.8, y: 0.0, width: 80.0, height: 100.0))
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = array[component][row]
        label.font = UIFont.systemFont(ofSize: 60)
        
        view.addSubview(label)
        
        
        return view
    }
   
}

